﻿(function () {
    'use strict';
    angular.module('fullmoon')
    .service('messageService', function () {
        var gs = {};
        gs.danger = function (message) {
            $.growl({
                message: gs.translate(message)
            }, {
                type: 'danger',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 5000,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                }
            });
        };

        gs.success = function (message) {
            $.growl({
                message: gs.translate(message)
            }, {
                type: 'success',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 2500,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                }
            });
        };

        gs.warning = function (message) {
            $.growl({
                message: gs.translate(message)
            }, {
                type: 'warning',
                className: 'btn-xs btn-inverse',
                placement: {
                    from: 'top',
                    align: 'right'
                },
                delay: 5000,
                animate: {
                    enter: 'animated bounceIn',
                    exit: 'animated bounceOut'
                },
                offset: {
                    x: 20,
                    y: 85
                }
            });
        };

        gs.translate = function (message) {
            var translate = $translate.instant(message);
            return translate || message;
        };

        return gs;
    })
})();