﻿(function () {
    'use strict';

    angular.module('fullmoon', ['ngMaterial', 'ngAnimate', 'ngRoute'])
        .config(function ($routeProvider, $mdProgressCircularProvider, $mdThemingProvider) {
            
            $mdThemingProvider.theme('customTheme')
                .primaryPalette('blue')
                .warnPalette('red');

            $routeProvider
                .when('/users', {
                    templateUrl: '../App/templates/manager/users.html',
                    controller: 'managerController',
                    controllerAs: 'ctrl'
                }).when('/roles', {
                    templateUrl: '../App/templates/manager/roles.html'
                }).otherwise({redirectTo: '/users'});
        });
})();