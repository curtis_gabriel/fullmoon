﻿(function () {
    'use strict';
    angular.module('fullmoon')
        .controller('managerController', ['$mdDialog', 'managerService', 'messageService', function ($mdDialog, managerService, messageService) {
            var self = this;

            self.users = [];

            self.roles = [];

            self.getUsers = function () {
                managerService.GetUsers()
                    .then(function (response) {
                        self.users = response;
                    }, function (response) {
                        console.log(response);
                    });
            };

            self.edit = function (user) {
                console.log(user);
            };

            self.addUser = function () {
                console.log(self.users);
            };

        }]);
})();