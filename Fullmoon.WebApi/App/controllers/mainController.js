﻿(function () {
    angular.module('fullmoon')
        .controller('mainController', function ($location) {
            var self = this;
            self.showUsers = function () {
                $location.path('users');
            };
            self.showRoles = function () {
                $location.path('roles');
            };
        });
})();