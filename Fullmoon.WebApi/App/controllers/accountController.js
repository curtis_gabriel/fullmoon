﻿(function () {
    angular.module('fullmoon')
        .controller('accountController', ['$mdDialog', '$rootScope', 'messageService', 'accountService', function ($mdDialog, $rootScope, messageService, accountService) {


            var vm = this;
            vm.loading = false;
            vm.account = {};

            vm.openAccessModal = function () {
                vm.loading = true;
                $rootScope.loading = true;
                $mdDialog.show({
                    templateUrl: '../App/templates/account/access.html',
                    controller: vm.accessController,
                    controllerAs: 'ctrl',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false
                });
            };

            var treatMessage = function (message) {
                if (message != null)
                    messageService.show(message.Text, message.Type);
            }

            vm.registerAccount = function (name, email, password) {
                vm.loading = true;
                accountService.RegisterAccount(name, email, password)
                    .then(function (response) {
                        vm.loading = false;
                        treatMessage(response.Message);
                    },
                    function (response) {
                        vm.loading = false;
                        console.log('http error in registerAccount');
                    });
            };

            vm.accessAccount = function (name, password) {
                vm.loading = true;
                accountService.AccessAccount(name, password)
                    .then(function (response) {                        
                        treatMessage(response.Message);
                        if (response.Logged) {
                            location.pathname = 'home/index';
                        } else {
                            vm.loading = false;
                        }
                    },
                    function (response) {
                        vm.loading = false;
                        console.log('http error in accessAccount');
                    });
            };

            vm.exit = accountService.Exit;

            vm.getAccountInfo = function () {
                vm.loading = true;
                accountService.GetAccountInfo()
                    .then(function (response) {
                        vm.loading = false;
                        vm.account = angular.copy(response);
                    }, function (response) {
                        vm.loading = false;
                        console.log('http error in getAccountInfo');
                    });
            };

            vm.openRegisterModal = function () {
                vm.loading = true;
                $mdDialog.show({
                    templateUrl: '../App/templates/account/register.html',
                    controller: vm.registerController,
                    controllerAs: 'ctrl',
                    parent: angular.element(document.body),
                    clickOutsideToClose: false
                });
            };

            vm.accessController = function () {
                vm.loading = false;
                var ac = this;
                ac.name = '';
                ac.password = '';
                ac.fieldsValid = false;

                ac.verifyFields = function () {
                    if (ac.name == null || ac.password == null)
                        ac.fieldsValid = false;
                    else
                        ac.fieldsValid = ac.name.length > 3 && ac.password.length > 5;
                };
                ac.openRegisterModal = vm.openRegisterModal;

                ac.access = function () {
                    vm.accessAccount(ac.name, ac.password);
                };
            };

            vm.registerController = function () {
                vm.loading = false;
                var rc = this;
                rc.name = '';
                rc.email = '';
                rc.password = '';
                rc.confirmPassword = '';
                rc.fieldsValid = false;

                rc.verifyFields = function () {
                    if (rc.name == null || rc.email == null || rc.password == null || rc.confirmPassword == null)
                        rc.fieldsValid = false;
                    else
                        rc.fieldsValid = rc.name.length > 3 && rc.email.length > 12 && rc.password.length > 5 && rc.password == rc.confirmPassword;
                };
                rc.openAccessModal = vm.openAccessModal;

                rc.register = function () {
                    vm.registerAccount(rc.name, rc.email, rc.password);
                };
            };
        }]);
})();