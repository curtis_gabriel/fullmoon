﻿var angular;
(function () {
    'use strict';
    console.log('app');
    angular
        .module('app', ['ngMaterial', 'ngAnimate'])
        .config(function ($mdThemingProvider) {

            /*$mdThemingProvider.theme('altTheme')
    			.primaryPalette('purple')

    			$mdThemingProvider.setDefaultTheme('altTheme');

    			*/
            // Extend the red theme with a few different colors
            var greenFieldMap = $mdThemingProvider.extendPalette('green', {
                '500': '1A592E'
            });
            // Register the new color palette map with the name <code>neonRed</code>
            $mdThemingProvider.definePalette('greenField', greenFieldMap);
            
            $mdThemingProvider.theme('default')
                .primaryPalette('greenField')
                .accentPalette('yellow');

        })
        .controller('appController', function ($scope, $mdDialog) {
            
            $scope.showAlert = function () {

                $mdDialog.show({
                    templateUrl: 'App/templates/login.html',
                    controller: loginController,
                    parent: angular.element(document.body),
                    clickOutsideToClose: true
                });
            }
        });
    function loginController($scope, $mdDialog) {
        $scope.formValid = false;
        $scope.name = '';
        $scope.password = '';
        $scope.verifyFields = function () {
            if ($scope.name != null && $scope.password != null) {
                $scope.formValid = $scope.name.length > 3 && $scope.password.length > 5;
            }
        }
    }
})();