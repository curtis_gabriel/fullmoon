﻿(function () {
    'use strict';
    angular.module('fullmoon')
        .factory('accountService', ['$http', '$q', function ($http, $q) {
            var service = {};
            service.URL = 'http://' + location.host;
            var _registerAccount = function (name, email, password) {
                var deferred = $q.defer();
                var query = service.URL + '/account/create';
                $http.post(query, { Name: name, Email: email, Password: password })
                    .success(function (response) {
                        deferred.resolve(response);
                    })
                    .error(function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            };

            service.RegisterAccount = _registerAccount;

            var _accessAccount = function (name, password) {
                var deferred = $q.defer();
                var query = service.URL + '/account/login';
                $http.post(query, { Name: name, Password: password })
                    .success(function (response) {
                        deferred.resolve(response);
                    })
                    .error(function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            }

            service.AccessAccount = _accessAccount;

            var _exit = function () {
                var query = service.URL + '/account/logout';
                $http.get(query)
                    .success(function () {
                        location.pathname = 'account/index';
                    });
            };

            service.Exit = _exit;

            var _getAccountInfo = function () {
                var deferred = $q.defer();
                var query = service.URL + '/account/GetAccountInfo';
                $http.get(query)
                    .success(function(response){ deferred.resolve(response); })
                    .error(function (response) { deferred.reject(response); });
                return deferred.promise;
            }

            service.GetAccountInfo = _getAccountInfo;

            return service;
        }]);
})();