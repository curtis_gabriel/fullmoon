﻿(function () {

    'use strict';
    angular.module('fullmoon')
    .service('messageService', function () {
        function Message() {

            var m = this;

            var mDiv = document.getElementById('message');
            var mLabel;
            if (mDiv == null) {
                mLabel = document.createElement('label');
                mDiv = document.createElement('div');
                mDiv.id = 'message';
                document.body.appendChild(mDiv);
                mDiv.appendChild(mLabel);
            } else
                mLabel = mDiv.children[0];

            var opacity = 0;
            var showing = false;

            var hide = function () {
                opacity -= 0.1;
                mDiv.style.opacity = opacity;
                if (!showing) {
                    if (opacity > 0)
                        setTimeout(hide, 50);
                    else
                        mDiv.style.display = 'none';
                }
            }

            var show = function () {
                if (opacity <= 0) {
                    mDiv.style.display = 'block';
                    showing = true;
                }
                opacity += 0.05;
                mDiv.style.opacity = opacity;
                if (opacity < .6)
                    setTimeout(show, 50);
                else {
                    showing = false;
                    setTimeout(initHide, 2500);
                }
            }

            var initShow = function (texto, type) {
                var backgroundColor;
                var fontColor;
                switch (type) {
                    case 1:
                        backgroundColor = '#C4EAB4';
                        fontColor = 'black';
                        break;
                    case 2:
                        backgroundColor = '#F6E8B1';
                        fontColor = 'black';
                        break;
                    case 3:
                        backgroundColor = '#F4B4B4';
                        fontColor = '#990000';
                        break;
                    default:
                        backgroundColor = 'gray';
                        fontColor = 'black';
                        break;
                }
                mDiv.style.backgroundColor = backgroundColor;
                mDiv.style.color = fontColor;
                mLabel.textContent = texto;
                opacity = 0;
                show();
            };

            var initHide = function () {
                if (!showing) {
                    opacity = .6;
                    hide();
                }
            };

            m.show = function (msg, type) {
                initShow(msg, type);
            }

            m.success = function (msg) {
                initShow(msg, 1);
            };
            
            m.warning = function (msg) {
                initShow(msg, 2);
            };

            m.error = function (msg) {
                initShow(msg, 3);
            };

            m.default = function (msg) {
                initShow(msg);
            }
        };

        return new Message();
    });
})();