﻿(function () {
    'use strict';
    angular.module('fullmoon')
        .factory('managerService', ['$http', '$q', function ($http, $q) {
            var service = {};

            service.URL = 'http://' + location.host;

            var _getUsers = function () {
                var deferred = $q.defer();
                var query = service.URL + '/api/user';
                $http.get(query)
                    .success(function (response) {
                        deferred.resolve(response);
                    }).error(function (response) {
                        deferred.reject(response);
                    });
                return deferred.promise;
            };

            service.GetUsers = _getUsers;

            return service;
        }]);
})();