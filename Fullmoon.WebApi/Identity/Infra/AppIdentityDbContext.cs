﻿using Microsoft.AspNet.Identity.EntityFramework;
using Fullmoon.WebApi.Identity.Models;
using System.Data.Entity;

namespace Fullmoon.WebApi.Identity.Infra
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext() : base("IdentityDb") { }

        static AppIdentityDbContext()
        {
            Database.SetInitializer<AppIdentityDbContext>(new IdentityDbInit());
        }

        public static AppIdentityDbContext Create()
        {
            return new AppIdentityDbContext();
        }
    }
}