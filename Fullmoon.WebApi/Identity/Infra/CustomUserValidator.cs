﻿using Fullmoon.WebApi.Identity.Models;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace Fullmoon.WebApi.Identity.Infra
{
    public class CustomUserValidator : UserValidator<AppUser>
    {
        public CustomUserValidator(AppUserManager mgr) : base(mgr) { }
        
        public override async Task<IdentityResult> ValidateAsync(AppUser user)
        {
            IdentityResult result = await base.ValidateAsync(user);
            if (!user.Email.ToLower().EndsWith("mail.com") && !user.Email.ToLower().EndsWith("mail.com.br"))
            {
                var errors = result.Errors.ToList();
                errors.Add("Only 'mail.com' email addresses are allowed");
                result = new IdentityResult(errors);
            }
            return result;
        }
    }
}