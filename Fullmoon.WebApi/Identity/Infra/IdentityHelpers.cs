﻿using Microsoft.AspNet.Identity.Owin;
using System.Web;
using System.Web.Mvc;

namespace Fullmoon.WebApi.Identity.Infra
{
    public static class IdentityHelpers
    {
        public static MvcHtmlString getUserName(this HtmlHelper html, string id)
        {
            AppUserManager mgr = HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>();
            return new MvcHtmlString(mgr.FindByIdAsync(id).Result.UserName);
        }
    }
}