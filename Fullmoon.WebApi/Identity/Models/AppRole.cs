﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace Fullmoon.WebApi.Identity.Models
{
    public class AppRole : IdentityRole
    {
        public AppRole() : base() { }

        public AppRole(string name) : base(name) { }
    }
}