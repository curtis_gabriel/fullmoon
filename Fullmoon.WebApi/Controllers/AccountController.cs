﻿using Fullmoon.WebApi.Identity.Infra;
using Fullmoon.WebApi.Identity.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using RequestDTO = Fullmoon.DTO.Request;
using ResponseDTO = Fullmoon.DTO.Response;

namespace Fullmoon.WebApi.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            else
                return View();
        }
        
        [HttpPost]
        public async Task<JsonResult> Login(RequestDTO.User userRequest)
        {
            ResponseDTO.UserDTO userResponse = new ResponseDTO.UserDTO();
            try
            {
                AppUser userFound = await UserManager.FindAsync(userRequest.Name, userRequest.Password);
                if (userFound == null)
                {
                    userResponse.Message = new ResponseDTO.MessageDTO("Invalid name or password.", ResponseDTO.TypeMessage.ERROR);
                }
                else
                {
                    userResponse.Name = userFound.UserName;
                    userResponse.Uid = userFound.Id;
                    userResponse.Email = userFound.Email;
                    userResponse.Logged = true;
                    ClaimsIdentity ident = await UserManager.CreateIdentityAsync(userFound, DefaultAuthenticationTypes.ApplicationCookie);
                    AuthManager.SignOut();
                    AuthManager.SignIn(new AuthenticationProperties { IsPersistent = false }, ident);
                }
                return Json(userResponse, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public void Logout()
        {
            AuthManager.SignOut();
        }

        [HttpPost]
        public async Task<JsonResult> Create(RequestDTO.User userDTO)
        {
            ResponseDTO.UserDTO userResponse = new ResponseDTO.UserDTO();
            try
            {
                AppUser user = new AppUser { UserName = userDTO.Name, Email = userDTO.Email };
                IdentityResult result = await UserManager.CreateAsync(user, userDTO.Password);
                if (result.Succeeded)
                {
                    userResponse.Name = user.UserName;
                    userResponse.Email = user.Email;
                    userResponse.Logged = true;
                    return Json(userResponse, JsonRequestBehavior.AllowGet);
                }
                foreach(var error in result.Errors)
                    userResponse.Message = new ResponseDTO.MessageDTO(error, ResponseDTO.TypeMessage.ERROR);
                
                return Json(userResponse, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }            
        }

        public async Task<JsonResult> GetAccountInfo()
        {
            var accountInfo = new ResponseDTO.AccountInfoDTO();
            try
            {
                var user = await UserManager.FindByNameAsync(AuthManager.User.Identity.Name);
                if(user != null)
                {
                    accountInfo.Uid = user.Id;
                    accountInfo.UserName = user.UserName;
                    accountInfo.Email = user.Email;
                    accountInfo.Roles = await UserManager.GetRolesAsync(user.Id);
                    
                }
                return Json(accountInfo, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                accountInfo.Message = new ResponseDTO.MessageDTO(ex.Message, DTO.Response.TypeMessage.ERROR);
                return Json(accountInfo, JsonRequestBehavior.AllowGet);
            }
        }

        private IAuthenticationManager AuthManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private AppUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        private AppRoleManager RoleManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }
    }
}