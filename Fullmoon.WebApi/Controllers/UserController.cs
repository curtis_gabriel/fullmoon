﻿using Response = Fullmoon.DTO.Response;
using Fullmoon.WebApi.Identity.Infra;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Fullmoon.WebApi.Controllers
{
    public class UserController : ApiController
    {
        public IList<Response.UserDTO> Get()
        {
            var usersLoaded = AppUserManager.Users.ToList();
            var users = new List<Response.UserDTO>();
            var roles = AppRoleManager.Roles.ToList();
            
            foreach (var user in usersLoaded)
            {
                var userDTO = new Response.UserDTO();
                userDTO.Uid = user.Id;
                userDTO.Name = user.UserName;
                userDTO.Email = user.Email;
                userDTO.Roles = new List<string>();

                user.Roles.ToList().ForEach(p => userDTO.Roles.Add(roles.FirstOrDefault(x => x.Id == p.RoleId).Name));
                
                users.Add(userDTO);
            }
            return users;
        }

        private AppUserManager AppUserManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        private AppRoleManager AppRoleManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().GetUserManager<AppRoleManager>();
            }
        }
    }
}
