﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fullmoon.DTO.Response
{
    public abstract class ResponseDTO
    {
        public MessageDTO Message { get; set; }
    }
}
