﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fullmoon.DTO.Response
{
    public class UserDTO : ResponseDTO
    {
        public string Uid { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public bool Logged { get; set; }

        public IList<string> Roles { get; set; }
    }
}
