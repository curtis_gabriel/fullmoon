﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fullmoon.DTO.Response
{
    public enum TypeMessage
    {
        SUCCESS,
        WARNING,
        ERROR,
        DEFAULT
    }

    public class MessageDTO
    {
        public MessageDTO(string text, TypeMessage type)
        {
            this.Text = text;
            switch (type)
            {
                case TypeMessage.DEFAULT:
                    this.Type = 0;
                    break;
                case TypeMessage.SUCCESS:
                    this.Type = 1;
                    break;
                case TypeMessage.WARNING:
                    this.Type = 2;
                    break;
                case TypeMessage.ERROR:
                    this.Type = 3;
                    break;
            }
        }

        public int Type { get; set; }

        public string Text { get; set; }
    }
}